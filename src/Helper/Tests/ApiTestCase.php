<?php

namespace App\Helper\Tests;

use App\Entity\Account\Account;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class ApiTestCase
 * @package Helps\Test
 */
class ApiTestCase extends WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    protected function getService($id)
    {
        return self::$container->get($id);
    }

    protected function getAccountInstance(
        array $findCriteria = ['email' => 'developer@app.com']
    ): Account {
        return $this->getService('doctrine')
            ->getRepository(Account::class)
            ->findOneBy($findCriteria);
    }

    /**
     * @return Client
     */
    protected function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param Account $account
     * @return Client
     */
    protected function login(Account $account): Client
    {
        $session = $this->getService('session');

        // the firewall context defaults to the firewall name
        $firewallContext = 'secured_area';

        $token = new UsernamePasswordToken(
            $account,
            null,
            $firewallContext,
            $account->getRoles()
        );

        $session->set('_security_' . $firewallContext, serialize($token));
        $session->save();

        $client = $this->getClient();

        $cookie = new Cookie($session->getName(), $session->getId());

        $client->getCookieJar()->set($cookie);

        return $client;
    }
}
