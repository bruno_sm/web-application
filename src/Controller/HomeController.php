<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 31/03/18
 * Time: 15:09
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="home")
     */
    public function home()
    {
        return $this->render('home.html.twig', [
            'account' => $this->getUser(),
        ]);
    }
}