<?php

namespace App\Controller\Api\V1;

use App\Api\V1\ControllerAbstract;
use App\Entity\Account\Account;
use App\Security\PasswordGenerator;
use App\Security\TokenGenerator;
use App\Validator\Validator;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AccountController
 * @package App\Controller\Api\V1
 * @Route("account")
 */
class AccountController extends ControllerAbstract
{
    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/", methods={"GET"}, name="get_account")
     */
    public function getAccountAction()
    {
        /** @var Account $account */
        $account = $this->getUser();

        return $this->createApiResponse([
            'uid' => $account->getUid(),
            'name' => $account->getName(),
            'email' => $account->getEmail(),
            'token' => $account->getToken(),
        ]);
    }

    /**
     * @param Request $request
     * @param Validator $validator
     * @param PasswordGenerator $passwordGenerator
     * @param ObjectManager $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/", methods={"POST"}, name="account_register")
     */
    public function register(
        Request $request,
        Validator $validator,
        PasswordGenerator $passwordGenerator,
        ObjectManager $em
    ) {
        $parameters = json_decode($request->getContent(), true);
        (is_array($parameters)) ?: $parameters = [];

        $parameters['plainPassword'] = $passwordGenerator->generatePassword();
        $parameters['roles'] = ['ROLE_API_V1'];

        /** @var Account $account */
        $account = Account::createFrom($parameters, $validator, ['registration']);

        $em->persist($account);
        $em->flush();

        return $this->createApiResponse(
            [
                'uid' => $account->getUid(),
                'email' => $account->getEmail(),
                'name' => $account->getName(),
                'plainPassword' => $account->getPlainPassword(),
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @param Request $request
     * @param Validator $validator
     * @param ObjectManager $em
     * @param SerializerInterface $serializer
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/", methods={"PUT"}, name="update_account");
     */
    public function updateAccount(
        Request $request,
        Validator $validator,
        ObjectManager $em,
        SerializerInterface $serializer
    ) {
        $parameters = json_decode($request->getContent(), true);
        (is_array($parameters)) ?: $parameters = [];

        unset(
            $parameters['id'],
            $parameters['uid'],
            $parameters['roles'],
            $parameters['token'],
            $parameters['created_at']
        );

        if (isset($parameters['password'])) {
            $parameters['plainPassword'] = $parameters['password'];
            unset($parameters['password']);
        }

        $attrs = json_decode(
            $serializer->serialize($this->getUser(), 'json'),
            true
        );
        $attrs = array_merge($attrs, $parameters);

        unset($attrs['salt'], $attrs['username']);

        $validationGroups = (isset($parameters['email']))
            ? ['default', 'emailUpdate']
            : ['default'];

        $account = Account::createFrom($attrs, $validator, $validationGroups);

        $em->merge($account);
        $em->flush();

        return $this->getAccountAction();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/{uid}", methods={"DELETE"}, name="account_delete", requirements={"uid": "[^/]+"})
     */
    public function deleteAccount($uid)
    {
        $em = $this->getDoctrine()
            ->getManager();

        $accountToRemove = $em->getRepository(Account::class)
            ->findOneBy(['uid' => $uid]);

        if (empty($accountToRemove)) {
            $this->throwApiProblemResponse(
                Response::HTTP_NOT_FOUND,
                ['Account not found.']
            );
        }

        $em->remove($accountToRemove);
        $em->flush();

        return $this->createApiResponse(
            '{}',
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * Generates an Account Token and updated the current Account
     *
     * @param TokenGenerator $tokenGenerator
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/token/", methods={"PUT"}, name="generate_token");
     */
    public function generateToken(TokenGenerator $tokenGenerator)
    {
        /** @var Account $account */
        $account = $this->getUser();

        $account->setToken($tokenGenerator->generateToken($account->getEmail()));
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->createApiResponse([
            'uid' => $account->getUid(),
            'token' => $account->getToken(),
        ]);
    }

    /**
     * Erase an Account Token
     *
     * @Route("/token/", methods={"DELETE"}, name="erase_token");
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function eraseToken()
    {
        /** @var Account $account */
        $account = $this->getUser();
        $account->clearToken();

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->createApiResponse('{}',Response::HTTP_NO_CONTENT);
    }
}