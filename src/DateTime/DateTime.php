<?php

namespace App\DateTime;

use DateTimeZone;

/**
 * Class DateTime
 * @package Entity\Common\DateTime
 */
class DateTime extends \DateTime implements \JsonSerializable
{
    /**
     * @inheritdoc
     */
    public function __construct($time = "now", DateTimeZone $timezone = null)
    {
        parent::__construct($time, self::getTimeZoneInstance($timezone));
    }

    /**
     * @return mixed|string
     */
    public function __toString()
    {
        return $this->jsonSerialize();
    }

    /**
     * @return mixed|string
     */
    public function jsonSerialize()
    {
        return $this->format(\DateTime::RFC3339);
    }

    /**
     * @param DateTimeZone|null $timezone
     * @return DateTimeZone
     */
    protected static function getTimeZoneInstance(DateTimeZone $timezone = null)
    {
        return ($timezone) ?? new DateTimeZone('UTC');
    }

    /**
     * @param string $format
     * @param string $time
     * @param null $timezone
     * @return DateTime
     */
    public static function createFromFormat($format, $time, $timezone = null)
    {
        $timezone = self::getTimeZoneInstance($timezone);

        $tmp = parent::createFromFormat($format, $time, $timezone);
        $dt = new DateTime($tmp->format(self::RFC3339), $timezone);

        unset($tmp);

        return $dt;
    }
}
