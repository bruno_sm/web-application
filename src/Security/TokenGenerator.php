<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 01/04/18
 * Time: 12:40
 */

namespace App\Security;


class TokenGenerator
{
    /**
     * @return string $token
     */
    public function generateToken(string $salt = null): string
    {
        return sha1($salt . microtime() . random_bytes(40));
    }
}