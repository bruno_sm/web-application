<?php

namespace App\Security;


class PasswordGenerator
{
    const CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-';

    const LENGTH = 10;

    /**
     * @var string
     */
    protected $chars;

    /**
     * @var int
     */
    protected $length;

    /**
     * PasswordGenerator constructor.
     * @param string|null $chars
     * @param int|null $length
     */
    public function __construct(string $chars = self::CHARS, int $length = self::LENGTH)
    {
        $this->chars = $chars;
        $this->length = $length;
    }

    /**
     * @return string
     */
    public function generatePassword(): string
    {
        $password = '';

        do {
            $password .= str_shuffle($this->chars);
        } while (strlen($password) <= $this->length);

        return substr($password, 0, $this->length);
    }
}