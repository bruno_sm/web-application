<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 26/01/18
 * Time: 08:14
 */

namespace App\Security;

use App\Api\V1\ApiProblem;
use App\Api\V1\ApiProblemException;
use App\Entity\Account\Account;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ApiTokenAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var EntityManagerInterface
     */
    protected $em = null;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function supports(Request $request)
    {
        return $request->headers->has('X-AUTH-TOKEN');
    }

    public function getCredentials(Request $request)
    {
        return $request->headers->get('X-AUTH-TOKEN');
    }

    public function getUser($token, UserProviderInterface $userProvider)
    {
        if (is_null($token)) {
            return;
        }

        return $this->em->getRepository(Account::class)
            ->findOneBy(['token' => $token]);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $apiProblem = new ApiProblem(
            Response::HTTP_FORBIDDEN,
            ['errors' => ['Authentication Required']]
        );

        $response = new JsonResponse(
            $apiProblem->toArray(),
            JsonResponse::HTTP_FORBIDDEN
        );

        $response->headers->set('Content-Type', 'application/problem+json');
        $response->headers->set('Content-Language', 'en');

        return $response;
    }

    public function supportsRememberMe()
    {
        return false;
    }
}