<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 01/04/18
 * Time: 15:04
 */

namespace App\DataFixtures\ORM;

use App\Entity\Account\Account;
use App\Security\PasswordGenerator;
use App\Security\TokenGenerator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Output\OutputInterface;

class AccountFixtures extends Fixture
{
    /**
     * @var PasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @var TokenGenerator
     */
    protected $tokenGenerator;

    /**
     * AccountFixtures constructor.
     * @param PasswordGenerator $passwordGenerator
     * @param TokenGenerator $tokenGenerator
     * @param OutputInterface $output
     */
    public function __construct(
        PasswordGenerator $passwordGenerator,
        TokenGenerator $tokenGenerator
    ) {
        $this->passwordGenerator = $passwordGenerator;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function load(ObjectManager $manager)
    {
        $accounts = [
            'developer' => $this->getAccountInstance('developer@app.com'),
            'tester' => $this->getAccountInstance('tester@app.com', 'tester'),
        ];

        print PHP_EOL;
        print '---------------------' . PHP_EOL;
        print '| Developer account |' . PHP_EOL;
        print '---------------------' . PHP_EOL;
        print 'Email: ' . $accounts['developer']->getEmail() . PHP_EOL;
        print 'Password: ' . $accounts['developer']->getPlainPassword() . PHP_EOL;
        print 'API token: ' . $accounts['developer']->getToken() . PHP_EOL;
        print PHP_EOL;

        foreach ($accounts as $key => $account) {
            $manager->persist($account);
        }

        $manager->flush();
    }

    protected function getAccountInstance($email, $password = null)
    {
        $account = new Account();

        $password = ($password) ?? $this->passwordGenerator->generatePassword();

        $account->setEmail($email)
            ->setRoles(['ROLE_DEVEL'])
            ->setPlainPassword($password)
            ->setToken($this->tokenGenerator->generateToken())
            ->setName('Developer');

        return $account;
    }
}