<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 18/05/17
 * Time: 15:36
 */

namespace App\EventListener;

use App\Api\V1\ApiProblem;
use App\Api\V1\ApiProblemException;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var bool
     */
    private $debug;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * ApiExceptionSubscriber constructor.
     * @param bool $debug
     * @param LoggerInterface $logger
     */
    public function __construct(bool $debug, LoggerInterface $logger, RouterInterface $router)
    {
        $this->debug = $debug;
        $this->logger = $logger;
        $this->router = $router;
    }

    public function processException(GetResponseForExceptionEvent $event)
    {
        $e = $event->getException();

        if ($this->debug) {
            return;
        }

        $apiProblem = $this->getApiProblemInstance($e);
        $data = $apiProblem->toArray();
        // making type a URL, to a temporarily fake page
        if ($data['type'] != 'about:blank') {
            $data['type'] = 'http://localhost:8000/docs/errors#' . $data['type'];
        }

        $response = new JsonResponse($data, $apiProblem->getStatusCode());
        $response->headers->set('Content-Type', 'application/problem+json');
        $response->headers->set('Content-Language', 'en');

        $event->setResponse($response);
    }

    protected function getApiProblemInstance(Exception $e)
    {
        if ($e instanceof ApiProblemException) {
            return $e->getApiProblem();
        }

        return ApiProblem::createFromException($e);
    }

    public function logException(GetResponseForExceptionEvent $event)
    {
        $e = $event->getException();

        $this->logger->log(Logger::ERROR, json_encode([
            'exception' => get_class($e),
            'message' => $e->getMessage(),
            'trace' => $e->getTrace(),
        ]));
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['processException', 10],
                ['logException', 0],
            ],
        ];
    }
}