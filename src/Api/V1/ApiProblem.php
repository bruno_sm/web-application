<?php

namespace App\Api\V1;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * A wrapper for holding data to be used for a application/problem+json response
 * @link https://tools.ietf.org/html/rfc7807 Problem Details for HTTP APIs
 */
class ApiProblem
{
    /**
     * @var array
     */
    protected static $data = [
        Response::HTTP_BAD_REQUEST => [
            'type'     => 'bad_request',
            'title'    => 'Bad request',
        ],
        Response::HTTP_INTERNAL_SERVER_ERROR => [
            'type'     => '',
        ],
        Response::HTTP_NOT_FOUND => [
            'type'     => '',
        ],
        Response::HTTP_FORBIDDEN => [
            'type'     => 'access_denied',
            'title'    => 'Access denied',
        ],
        Response::HTTP_CONFLICT => [
            'type'     => 'conflict',
            'title'    => 'The request generated a conflict',
        ],
    ];

    protected static $map = [
        \InvalidArgumentException::class => Response::HTTP_BAD_REQUEST,
        ValidatorException::class => Response::HTTP_BAD_REQUEST,
        AccessDeniedException::class => Response::HTTP_FORBIDDEN,
        AuthenticationException::class => Response::HTTP_FORBIDDEN,
    ];

    /**
     * @var array
     */
    protected $extraData = [];

    /**
     * @var int
     */
    protected $exceptionCode;

    /**
     * @var string
     */
    protected $title;

    /**
     * ApiProblem constructor.
     * @param int $exceptionCode
     * @param array $extraData
     */
    public function __construct(
        int $exceptionCode = Response::HTTP_BAD_REQUEST,
        array $extraData = []
    ) {
        if ($exceptionCode == 0) {
            $exceptionCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        if (! isset(self::$data[$exceptionCode])) {
            throw new \InvalidArgumentException(
                "Unknown the problem error code: " . $exceptionCode
            );
        }

        if (isset($extraData['title'])) {
            $this->title = $extraData['title'];
        } elseif (isset(self::$data[$exceptionCode]['title'])) {
            $this->title = self::$data[$exceptionCode]['title'];
        } else {
            $this->title = Response::$statusTexts[$exceptionCode];
        }

        $this->exceptionCode = $exceptionCode;
        $this->extraData = $extraData;
    }

    public static function createFromException(\Exception $e)
    {
        foreach (self::$map as $exception => $code) {
            if ($e instanceof $exception) {
                return new static($code, ['detail' => $e->getMessage()]);
            }
        }

        return new static(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $type = self::$data[$this->exceptionCode]['type'];

        /*
         * When "about:blank" is used, the title SHOULD be the same as the
         * recommended HTTP status phrase for that code (e.g., "Not Found" for
         * 404, and so on), although it MAY be localized to suit client
         * preferences (expressed with the Accept-Language request header).
         */
        if ((empty($type)) || ($type == 'about:blank')) {
            $type  = 'about:blank';
            $this->title = 'Not Found';
        }

        return array_merge(
            [
                'type'   => $type,
                'title'  => $this->getTitle(),
                'status' => $this->getStatusCode(),
            ],
            $this->extraData
        );
    }

    /**
     * @param string $name
     * @param mixed $value
     *
     * @return $this
     *
     */
    public function set($name, $value)
    {
        $this->extraData[$name] = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->exceptionCode;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::$data[$this->exceptionCode]['type'];
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}
