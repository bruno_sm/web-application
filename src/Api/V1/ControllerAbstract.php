<?php

namespace App\Api\V1;

use App\Entity\Account\Account;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class ControllerAbstract extends Controller
{
    /**
     * @param Account $account
     * @param Request $request
     * @return $this
     */
    public function loginUser(Account $account, Request $request)
    {
        $this->get('security.authentication.guard_handler')
            ->authenticateUserAndHandleSuccess(
                $account,
                $request,
                $this->get('app.security.login_form_authenticator'),
                'main'
            );

        return $this;
    }

    /**
     * @param $data
     * @param int $statusCode
     * @param array $headers
     * @return JsonResponse
     */
    protected function createApiResponse(
        $data,
        int $statusCode = Response::HTTP_OK,
        $headers = []
    ): JsonResponse {
        return new JsonResponse(
            $data,
            $statusCode,
            array_merge(
                $headers,
                [
                    'Content-Type' => 'application/json',
                    'Content-Language' => 'en',
                ]
            )
        );
    }

    /**
     * @param int $exceptionCode
     * @param array $errors
     * @param array $extraData
     * @return JsonResponse
     */
    protected function throwApiProblemResponse(
        int $exceptionCode,
        array $errors = [],
        array $extraData = []
    ): JsonResponse {
        $extraData = array_merge($extraData, [ 'errors' => $errors ]);
        $apiProblem = new ApiProblem(
            $exceptionCode,
            $extraData
        );

        throw new ApiProblemException($apiProblem, null, [], $exceptionCode);
    }
}
