<?php
/**
 * Created by IntelliJ IDEA.
 * User: bruno.miranda
 * Date: 27/09/18
 * Time: 07:30
 */

namespace App\Entity;

use InvalidArgumentException;
use ReflectionClass;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

trait SelfCreatorTrait
{
    /**
     * @param ReflectionClass $reflector
     * @param object $object
     * @param array $values
     * @return object
     */
    private static function hydrateAttributes(
        ReflectionClass $reflector,
        object $object,
        array $values
    ): object {
        foreach ($values as $key => $value) {
            if (!property_exists($object, $key)) {
                $msg = sprintf('Unknown attribute "%s".', $key);
                throw new InvalidArgumentException($msg);
            }

            $property = $reflector->getProperty($key);
            $property->setAccessible(true);
            $property->setValue($object, $value);
        }

        return $object;
    }

    /**
     * @param object $object
     * @param ValidatorInterface $validator
     * @param array|null $validationGroups
     */
    private static function validate(
        object $object,
        ValidatorInterface $validator,
        array $validationGroups = null
    ) {
        $errors = $validator->validate($object, null, $validationGroups);
        $result = [];

        foreach ($errors as $error) {
            /** @var ConstraintViolationInterface $error */
            $result[] = $error->getPropertyPath() . ': ' . $error->getMessage();
        }

        if (empty($result)) {
            return;
        }

        throw new ValidatorException(implode(' / ', $result));
    }

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * @param array $attrs
     * @param ValidatorInterface|null $validator
     * @param array $validationGroups
     * @return object
     */
    public static function createFrom(
        array $attrs,
        ValidatorInterface $validator = null,
        array $validationGroups = null
    ) {
        /** @noinspection PhpUnhandledExceptionInspection */
        $reflector = new ReflectionClass(static::class);
        $object = $reflector->newInstanceWithoutConstructor();

        self::hydrateAttributes($reflector, $object, $attrs);

        if ($validator) {
            self::validate($object, $validator, $validationGroups);
        }

        return $object;
    }
}