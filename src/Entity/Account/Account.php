<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 31/03/18
 * Time: 16:10
 */

namespace App\Entity\Account;

use App\DateTime\DateTime;
use App\Entity\SelfCreatorTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class Account
 * @package App\Entity\Account
 * @ORM\Entity
 * @ORM\Table(name="account")
 * @UniqueEntity(fields={"email"}, groups={"registration", "emailUpdate"}, message="It looks like you already have an account.")
 */
class Account implements UserInterface
{
    use SelfCreatorTrait { createFrom as protected traitCreateFrom; }

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"registration", "default", "emailUpdate"})
     * @Assert\Email(groups={"registration", "default", "emailUpdate"})
     * @ORM\Column(type="string", length=64, nullable=false, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="json_array", nullable=false)
     */
    protected $roles = [];

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=64,
     *     maxMessage="The ""name"" field is too long. It should have {{ limit }} character or less."
     * )
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $password;

    /**
     * @Assert\NotBlank(groups={"registration"})
     */
    protected $plainPassword;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=64, unique=true, nullable=false)
     */
    protected $uid;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var string
     * @Assert\Length(max=64)
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $token;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public static function createFrom(
        array $attrs,
        ValidatorInterface $validator = null,
        array $validationGroups = null
    ) {
        if (isset($attrs['createdAt'])) {
            $attrs['createdAt'] = DateTime::createFromFormat(
                DateTime::RFC3339,
                $attrs['createdAt']
            );
        }

        ($attrs['createdAt'] instanceOf DateTime) ?: $attrs['createdAt'] = new DateTime();

        return self::traitCreateFrom($attrs, $validator, $validationGroups);
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->hasRole('ROLE_USER')
            ? $this->roles
            : array_merge(['ROLE_USER'], $this->roles);
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     * @return Account
     */
    public function setUid(string $uid): Account
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @param array $roles
     * @return Account
     */
    public function setRoles(array $roles): Account
    {
        $this->roles = array_values($roles);
        return $this;
    }

    /**
     * @param string $role
     * @return Account
     */
    public function addRole(string $role): Account
    {
        if (!$this->hasRole($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @param string $role
     * @return bool
     */
    public function removeRole(string $role): bool
    {
        $size = count($this->roles);

        $filter = function ($value) use ($role) {
            return ($value != $role);
        };

        $this->setRoles(array_filter($this->roles, $filter));
        return ($size > count($this->roles));
    }

    /**
     * @param string $role
     * @return bool
     */
    public function hasRole(string $role): bool
    {
        return in_array($role, $this->roles, true);
    }

    /**
     * @return null|string|void
     */
    public function getSalt()
    {
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Account
     */
    public function setName($name): Account
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return $this
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
        return $this;
    }


    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Account
     */
    public function setEmail($email): Account
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Account
     */
    public function setPassword(string $password): Account
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return Account
     */
    public function setToken(string $token): Account
    {
        $this->token = $token;
        return $this;
    }

    public function clearToken(): Account
    {
        $this->token = null;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     * @return Account
     */
    public function setPlainPassword(string $plainPassword): Account
    {
        // The password attribute is updated by HashPasswordListener.
        // This line is need to tell doctrine to update this entity and dispatch an update event.
        $this->password = null;
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}