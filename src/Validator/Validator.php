<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 01/04/18
 * Time: 14:13
 */

namespace App\Validator;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\TraceableValidator;

class Validator extends TraceableValidator
{
    /**
     * @var ConstraintViolationListInterface
     */
    protected $errors = null;

    public function validate($value, $constraints = null, $groups = null)
    {
        $this->errors = parent::validate($value, $constraints, $groups);
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getLastErrorsAsArray(): array
    {
        $errors = [];

        if (! $this->errors) {
            return $errors;
        }

        $count = $this->errors->count();
        for ($i = 0; $i < $count; $i++) {
            $errors[] = [
                'name' => $this->errors->get($i)->getPropertyPath(),
                'value' => $this->errors->get($i)->getInvalidValue(),
                'message' => $this->errors->get($i)->getMessage(),
            ];
        }

        return $errors;
    }
}