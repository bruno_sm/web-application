<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class LoginForm
 * @package EssentialsBundle\Form
 */
class LoginForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_account', EmailType::class,[
                'attr' => [
                    'class' => 'border-focus-blue form-control',
                    'placeholder' => 'user@youremail.com',
                ],
            ])
            ->add('_password', PasswordType::class, [
                'attr' => [
                    'class' => 'border-focus-blue form-control',
                    'placeholder' => 'password',
                ],
            ])
            ->add('login', SubmitType::class, [
                'label' => 'Login',
                'attr' => [
                    'class' => 'float-right btn btn-dark',
                ]
            ]);
    }
}
