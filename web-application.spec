%define _package_name web-application
%define _rootdir /usr/local/web-application

Summary: Web application system
Name: %{_package_name}
Version: 0.0.0
Release: 0.el%{rhel}
License: WTFPL
URL: https://bitbucket.org/bruno_sm/web-application/wiki/Home
Packager: Bruno Miranda <brunodes.miranda@gmail.com>
Vendor: Bruno Miranda, https://bitbucket.org/bruno_sm/web-application/wiki/Home
Source: %{_package_name}-%{version}-%{release}.tgz
BuildArch: noarch
BuildRoot: %{_tmppath}/%{_package_name}-%{version}-%{release}-root
Requires: php71-runtime, php71-php, php71-php-xml, php71-php-process, php71-php-mysqlnd, php71-php-pecl-apcu, php71-php-json, php71-php-cli, php71-php-mbstring, php71-php-opcache, php71-php-common, php71-php-pdo, php71-php-pecl-zip, httpd, mod_ssl, redis, MariaDB-server >= 10.2, MariaDB-client, epel-release
Provides: %{_package_name}
BuildRequires: php71-php

%description
Web application system

%prep
%setup
%build

%install
%{__mkdir} -p %{buildroot}%{_rootdir}/
%{__mkdir} -p %{buildroot}/usr/bin/

%{__cp} -a * %{buildroot}%{_rootdir}/

%pre

%post
cat <<EOF > %{_rootdir}/.env
# This file is a "template" of which env vars need to be defined for your application
# Copy this file to .env file for development, create environment variables when deploying to production
# https://symfony.com/doc/current/best_practices/configuration.html#infrastructure-related-configuration

###> symfony/framework-bundle ###
APP_ENV=dev
APP_SECRET=d2d0770a32cfdf0e31bb2f2c78f3dcd7
#TRUSTED_PROXIES=127.0.0.1,127.0.0.2
#TRUSTED_HOSTS=localhost,example.com
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# Configure your db driver and server_version in config/packages/doctrine.yaml
DATABASE_URL=mysql://app-user:app-pass@127.0.0.1:3306/app
###< doctrine/doctrine-bundle ###

EOF

[ ! -f /usr/bin/php ] && ln -s /usr/bin/php71 %{buildroot}/usr/bin/php 2>&-

%preun

%postun

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, apache, apache, 0755)
%{_rootdir}

%changelog