<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 08/04/18
 * Time: 08:36
 */

namespace App\Security;

use PHPUnit\Framework\TestCase;

class TokenGeneratorTest extends TestCase
{
    /**
     * @test
     */
    public function generateToken()
    {
        $tokens = [];
        $generator = new TokenGenerator();

        for ($i = 0; $i < 20; $i++) {
            $token = $generator->generateToken();
            $this->assertFalse(isset($tokens[$token]));
            $tokens[$token] = true;
        }
    }
}
