<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 07/04/18
 * Time: 17:15
 */

namespace App\Security;

use PHPUnit\Framework\TestCase;

class PasswordGeneratorTest extends TestCase
{
    /**
     * @test
     * @dataProvider generatePasswordDataProvider
     */
    public function generatePassword($length, $chars)
    {
        $password = (new PasswordGenerator($chars, $length))
            ->generatePassword();

        $pattern = '/^(' . implode('|', str_split($chars)) . ')+$/';

        $this->assertEquals($length, strlen($password));
        $this->assertRegExp($pattern, $password);
    }

    public function generatePasswordDataProvider()
    {
        return [
            [7, 'abc'],
            [4, '98765ewerfgv bnhjui-ioabc'],
            [13, 'qzedcsyduhcbevri38497912dj _%'],
        ];
    }
}
