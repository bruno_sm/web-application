<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 08/04/18
 * Time: 10:01
 */

namespace App\Controller\Api\V1;

use App\Entity\Account\Account;
use App\Helper\Tests\ApiTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class AccountControllerTest extends ApiTestCase
{
    /**
     * @test
     **/
    public function getAccountForbidden()
    {
        $client = $this->getClient();

        $client->request('GET', '/api/v1/account/');
        $response = $client->getResponse();

        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );
    }

    /**
     * @test
     **/
    public function getAccountApi()
    {
        $account = $this->getAccountInstance();
        $client = $this->getClient();
        $headers = [ 'HTTP_X-AUTH-TOKEN' => $account->getToken() ];

        $client->request('GET', '/api/v1/account/', [], [], $headers);
        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertArrayHasKey('uid', $data);
        $this->assertArrayHasKey('email', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('token', $data);
        $this->assertInternalType('string', $data['uid']);
        $this->assertInternalType('string', $data['email']);
        $this->assertInternalType('string', $data['name']);
    }

    /**
     * @test
     **/
    public function getAccountSession()
    {
        $client = $this->login($this->getAccountInstance());

        $client->request('GET', '/web/v1/account/');
        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertArrayHasKey('uid', $data);
        $this->assertArrayHasKey('email', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('token', $data);
        $this->assertInternalType('string', $data['uid']);
        $this->assertInternalType('string', $data['email']);
        $this->assertInternalType('string', $data['name']);
    }

    /**
     * @test
     * @dataProvider badAccountsDataProvider
     **/
    public function registerBadRequest($content)
    {
        $client = $this->login($this->getAccountInstance());
        $client->request(
            'POST',
            '/web/v1/account/',
            [],
            [],
            [],
             $content
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function badAccountsDataProvider()
    {
        return [
            [ '{"name":"Tester","email":"skjk"}' ],
            [ '{"name":"Tester"}' ],
        ];
    }

    /**
     * @test
     **/
    public function register()
    {
        $client = $this->login($this->getAccountInstance());
        $client->request(
            'POST',
            '/web/v1/account/',
            [],
            [],
            [],
            '{"name":"Temporary user","email":"test@unit.com"}'
        );

        $response = $client->getResponse();
        $data = json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('email', $data);
        $this->assertArrayHasKey('uid', $data);
        $this->assertArrayHasKey('plainPassword', $data);

        $this->assertInternalType('string', $data['name']);
        $this->assertInternalType('string', $data['email']);
        $this->assertInternalType('string', $data['uid']);
        $this->assertInternalType('string', $data['plainPassword']);
    }

    /**
     * @test
     * @depends register
     */
    public function changePassword()
    {
        /** @var EntityManagerInterface $em */
        $em = $this->getService('doctrine')
            ->getManager();

        $repo = $em->getRepository(Account::class);

        $account = $repo->findOneBy(['email' => 'test@unit.com']);


        $client = $this->login($account);

        $this->assertFalse(password_verify('pass', $account->getPassword()));

        $client->request(
            'PUT',
            '/web/v1/account/',
            [],
            [],
            [],
            '{"password":"pass"}'
        );

        $em->detach($account);
        $account = $repo->findOneBy(['email' => 'test@unit.com']);

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertTrue(password_verify('pass', $account->getPassword()));

    }

    /**
     * @test
     * @depends register
     */
    public function updateAccountBadRequest()
    {
        $account = $this->getAccountInstance(['email' => 'test@unit.com']);
        $client = $this->login($account);

        $client->request(
            'PUT',
            '/web/v1/account/',
            [],
            [],
            [],
            '{"email":""}'
        );

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    /**
     * @test
     * @depends register
     */
    public function generateToken()
    {
        $account = $this->getAccountInstance(['email' => 'test@unit.com']);

        $oldToken = $account->getToken();

        $client = $this->login($account);
        $client->request('PUT', '/web/v1/account/token/');
        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('uid', $data);
        $this->assertArrayHasKey('token', $data);

        $token = $data['token'];

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertNotEmpty($token);
        $this->assertInternalType('string', $token);
        $this->assertNotEquals($oldToken, $token);
    }

    /**
     * @test
     * @depends generateToken
     **/
    public function eraseToken()
    {
        $account = $this->getAccountInstance(['email' => 'test@unit.com']);
        $client = $this->login($account);
        $client->request('DELETE', '/web/v1/account/token/');
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());

        $client->request('GET', '/web/v1/account/');
        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);
        $this->assertEmpty($data['token']);
    }

    /**
     * @test
     **/
    public function deleteAccount()
    {
        $accountToRemove = $this->getAccountInstance(['email' => 'test@unit.com']);

        $client = $this->login($this->getAccountInstance());
        $client->request(
            'DELETE',
            '/web/v1/account/' . $accountToRemove->getUid()
        );

        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode()
        );
    }

    /**
     * @test
     **/
    public function deleteInvalidAccount()
    {
        $client = $this->login($this->getAccountInstance());
        $client->request(
            'DELETE',
            '/web/v1/account/invalidAccountId'
        );

        $response = $client->getResponse();
        $this->assertEquals(
            Response::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
    }
}
