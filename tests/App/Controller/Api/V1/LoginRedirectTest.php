<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 09/04/18
 * Time: 08:44
 */

namespace App\Tests\App\Controller\Api\V1;

use App\Helper\Tests\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class LoginRedirectTest extends ApiTestCase
{
    /**
     * @test
     **/
    public function getAccountActionForbiddenApi()
    {
        $client = $this->getClient();

        $client->request('GET', '/api/v1/account/');
        $response = $client->getResponse();

        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );

        $this->assertTrue($response->headers->contains(
            'Content-Type',
            'application/problem+json'
        ));
    }

    /**
     * @test
     **/
    public function getAccountActionForbiddenWeb()
    {
        $client = $this->getClient();

        $client->request('GET', '/web/v1/account/');
        $response = $client->getResponse();

        $this->assertEquals(
            Response::HTTP_FOUND,
            $response->getStatusCode()
        );

        $this->assertTrue($response->headers->contains('location', '/login'));
        $this->assertTrue($response->headers->contains(
            'Content-Type',
            'application/json'
        ));
    }

    /**
     * @test
     **/
    public function getAccountActionForbiddenWebXhr()
    {
        $client = $this->getClient();
        $headers = [ 'HTTP_X-Requested-With' => 'XMLHttpRequest' ];

        $client->request('GET', '/web/v1/account/', [], [], $headers);
        $response = $client->getResponse();

        $this->assertEquals(
            Response::HTTP_FORBIDDEN,
            $response->getStatusCode()
        );

        $this->assertTrue($response->headers->contains(
            'Content-Type',
            'application/problem+json'
        ));
    }
}