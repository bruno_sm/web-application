<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 09/04/18
 * Time: 22:38
 */

namespace App\Controller;

use App\Helper\Tests\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class HomeControllerTest extends ApiTestCase
{
    /**
     * @test
     **/
    public function home()
    {
        $client = $this->login($this->getAccountInstance());
        $client->request('GET', '/');
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertTrue($response->headers->contains(
            'Content-Type',
            'text/html; charset=UTF-8'
        ));
    }
}
