<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 09/04/18
 * Time: 20:10
 */

namespace App\Controller\Security;

use App\Helper\Tests\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends ApiTestCase
{
    /**
     * @test
     **/
    public function alreadyLoggedDoingLoginRequest()
    {
        $client = $this->login($this->getAccountInstance());
        $client->request('GET', '/login');

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FOUND, $response->getStatusCode());
        $this->assertTrue($response->headers->contains(
            'Content-Type',
            'text/html; charset=UTF-8'
        ));
        $this->assertTrue($response->headers->contains('Location', '/'));
    }

    /**
     * @test
     * @dataProvider loginDataProvider
     */
    public function loginAction($email, $password, $endLocation)
    {
        $client = $this->getClient();

        $client->request('GET', '/');
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_FOUND, $response->getStatusCode());
        $this->assertTrue($response->headers->contains('Location', '/login'));

        $crawler = $client->request('GET', '/login');

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $form = $crawler->selectButton('Login')->form();

        $token = $crawler->filter('#login_form__token')
            ->extract(['value']);

        $form['login_form[_account]'] = $email;
        $form['login_form[_password]'] = $password;
        $form['login_form[_token]'] = $token[0];

        $client->submit($form);

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_FOUND, $response->getStatusCode());
        $this->assertTrue($response->headers->contains('Location', $endLocation));
    }

    public function loginDataProvider()
    {
        return [
            [
                'tester@app.com',
                'tester',
                '/',
            ],
            [
                'tester@app.com',
                'wrong password',
                '/login',
            ],
            [
                'tester@app.com',
                '',
                '/login',
            ],
            [
                '',
                'tester',
                '/login',
            ],
        ];
    }
}
