<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 14/04/18
 * Time: 14:27
 */

namespace App\DateTime;

use PHPUnit\Framework\TestCase;

class DateTimeTest extends TestCase
{

    /**
     * @test
     **/
    public function jsonSerialize()
    {
        $datetime1 = new DateTime();
        $json = json_encode($datetime1);

        $datetime2 = DateTime::createFromFormat(
            DateTime::RFC3339,
            json_decode($json)
        );

        $this->assertEquals(
            $datetime1->getTimestamp(),
            $datetime2->getTimestamp()
        );

        $this->assertEquals(
            json_decode(json_encode($datetime1)),
            (string)$datetime2
        );
    }

    /**
     * @test
     */
    public function toString()
    {
        $datetime1 = new DateTime();

        $datetime2 = DateTime::createFromFormat(
            DateTime::RFC3339,
            (string)$datetime1
        );

        $this->assertEquals(
            $datetime1->getTimestamp(),
            $datetime2->getTimestamp()
        );
    }

    /**
     * @test
     */
    public function jsonSerializeAndStringCastHaveTheSameFormat()
    {
        $datetime = new DateTime();

        $this->assertEquals(
            json_decode(json_encode($datetime)),
            (string)$datetime
        );
    }

    /**
     * @test
     */
    public function defaultTimeZone()
    {
        $this->assertEquals(
            'UTC',
            (new DateTime())->getTimezone()->getName()
        );
    }

    /**
     * @test
     */
    public function customTimeZone()
    {
        $datetime = new DateTime(
            'now',
            new \DateTimeZone('	America/Sao_Paulo')
        );

        $this->assertEquals(
            'America/Sao_Paulo',
            $datetime->getTimezone()->getName()
        );
    }
}
