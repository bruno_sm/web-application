<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 08/04/18
 * Time: 08:48
 */

namespace App\Api\V1;


use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class ApiProblemTest extends TestCase
{
    /**
     * @test
     * @dataProvider problemsDataProvider
     */
    public function toArray($code, $extraData)
    {
        $problem = new ApiProblem($code, $extraData);
        $arr = $problem->toArray();

        $this->assertInternalType('string', $problem->getType());
        $this->assertArrayHasKey('type', $arr);
        $this->assertArrayHasKey('title', $arr);

        if (isset($extraData['title'])) {
            $this->assertEquals($extraData['title'], $arr['title']);
        }

        $this->assertArrayHasKey('status', $arr);
        $this->assertEquals($code, $arr['status']);

        $extraData = array_filter($extraData, function ($key) {
            return (!in_array($key, ['type', 'title', 'status'], true));
        }, ARRAY_FILTER_USE_KEY);

        $this->assertCount(count($extraData) + 3, $arr);

        foreach ($arr as $key => $value) {
            if (! in_array($key, ['type', 'title', 'status'])) {
                $this->assertEquals($extraData[$key], $value);
            }
        }
    }

    public function problemsDataProvider()
    {
        return [
            [
                'code' => Response::HTTP_FORBIDDEN,
                'extraData' => [],
            ],
            [
                'code' => Response::HTTP_BAD_REQUEST,
                'extraData' => [
                    'title' => 'custom title',
                    'error' => 'unit test 1',
                ],
            ],
            [
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'extraData' => ['unit test 2', 'unit test 3'],
            ],
        ];
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function unknownApiProblemCode()
    {
        new ApiProblem(Response::HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL);
    }

    /**
     * @test
     */
    public function genericExceptionResponseCode()
    {
        $this->assertEquals(
            Response::HTTP_INTERNAL_SERVER_ERROR,
            (new ApiProblem(0))->getStatusCode()
        );
    }

    /**
     * @test
     */
    public function customField()
    {
        $type = 'my custom api problem field';

        $problem = new ApiProblem();
        $problem->set('field', $type);

        $arr = $problem->toArray();

        $this->assertArrayHasKey('field', $arr);
        $this->assertEquals($type, $arr['field']);
    }
}
