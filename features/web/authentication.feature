Feature: Authentication
  In order to gain access to the site management area
  As an admin
  I need to be able to login

  Scenario: Logging in
    Given there is an admin account "admin@app.com" with password "admin"
    And I am on "/"
    And I fill in "user@youremail.com" with "admin@app.com"
    And I fill in "password" with "admin"
    And I press "Login"
    Then I should see "Welcome"
